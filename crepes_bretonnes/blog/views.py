from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, reverse
from datetime import datetime
from django.utils.text import Truncator

def home(request):
    return HttpResponse("""
        <h1>Bienvenue sur mon blog !</h1>
        <p>Les crêpes bretonnes ça tue des mouettes en plein vol !</p>
    """)

def redirect_home(request, sub_path):
    # if sub_path == 'lol':
        return reverse(home)
    

def view_article(request, id_article):
    """ 
    Vue qui affiche un article selon son identifiant (ou ID, ici un numéro)
    Son ID est le second paramètre de la fonction (pour rappel, le premier
    paramètre est TOUJOURS la requête de l'utilisateur)
    """
    # Si l'ID est supérieur à 100, nous considérons que l'article n'existe pas
    print('lol', type(id_article))

    text = "Jack London naît à San Francisco. Trente ans plus tard, sa maison natale, au numéro 615 Third Street9, est détruite lors du séisme de 1906. Une plaque y a été posée en 1953 par la Société Historique de Californie (California Historical Society)."

    class Article:
        def __init__(self, author, title, content, date):
         self.author = author
         self.title = title
         self.content = content
         self.date = date
    article = Article('London', 'Martin Eden', Truncator(text).chars(80),datetime.now())

    if int(id_article) == 10:
        return render(request, 'blog/article.html', locals())

    # Il veut des articles ? Soyons fourbe et redirigeons-le vers djangoproject.com
    if id_article == 'lol':
        return redirect(view_article, id_article=42)

    if int(id_article) > 30 and int(id_article) < 40:
        return redirect("https://www.djangoproject.com")

    if int(id_article) > 100:
        raise Http404

    return HttpResponse(
        "Vous avez demandé l'article n° {0} !".format(id_article)    
    )

def list_articles(request, year, month=1):
    """ Liste des articles d'un mois précis. """
    return HttpResponse(
        "Vous avez demandé les articles de {0} {1}.".format(month, year)  
    )

def list_articles_by_tag(request, tag):
    """ Liste des articles d'un mois précis. """
    return HttpResponse(
        "Vous avez demandé les articles de {0}".format(tag)  
    )


def date_actuelle(request, pseudo='pikachu'):
    return render(request, 'blog/date.html', {'pseudo': pseudo, 'date': datetime.now()})


def addition(request, nombre1, nombre2):    
    total = nombre1 + nombre2

    # Retourne nombre1, nombre2 et la somme des deux au tpl
    return render(request, 'blog/addition.html', locals())